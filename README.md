PongJS
======

This is a pong game I made using HTML5, during boring school lessons ;-) and spare time.

Demo
----

You can find a PongJS demo at [my website](https://elinvention.ovh/pagine/pong.html).

Usage
-------------

1. Get the canvas to draw on `var canvas = document.getElementById("pong");`
2. create a Pong object `var pong = new Pong(canvas, '');`
3. Have fun!


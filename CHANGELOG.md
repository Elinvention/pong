Changelog
=========

Version 0.9.3 (21/12/2015)
--------------------------

* Prevent the next match from starting unintentionally.

Version 0.9.2 (31/10/2015)
--------------------------

* Full-screen mode.
* Reset score button.
* Improved performance.

Version 0.9.1
-------------

* Each player has to press UP or DOWN to begin the match instead of waiting for a countdown.
* Improved settings.
* Added this Changelog.


/*
@licstart
Pong - A very simple old school pong game implementented in HTML5.
Copyright (C) 2014 Elia Argentieri

	The JavaScript code in this page is free software: you can
	redistribute it and/or modify it under the terms of the GNU
	General Public License (GNU GPL) as published by the Free Software
	Foundation, either version 3 of the License, or (at your option)
	any later version.  The code is distributed WITHOUT ANY WARRANTY;
	without even the implied warranty of MERCHANTABILITY or FITNESS
	FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

	As additional permission under GNU GPL version 3 section 7, you
	may distribute non-source (e.g., minimized or compacted) forms of
	that code without the copy of the GNU GPL normally required by
	section 4, provided you include this license notice and a URL
	through which recipients can access the Corresponding Source.
@licend
*/


// Cross-browser support for requestAnimationFrame
var w = window;
requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

// Cross-browser support for requestFullScreen
function makeFullscreen(element)
{
	if (element.requestFullscreen) {
		element.requestFullscreen();
	} else if (element.msRequestFullscreen) {
		element.msRequestFullscreen();
	}else if (element.mozRequestFullScreen) {
		element.mozRequestFullScreen();
	}else if (element.webkitRequestFullscreen) {
		element.webkitRequestFullscreen();
	}
}

function map(x, in_min, in_max, out_min, out_max) {	//remaps given x value between a min and max
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomArbitrary(min, max) {
	return Math.random() * (max - min) + min;
}

function rad2deg(rad) {
	return rad * 180 / Math.PI;
}


function Settings() {
	var settings = document.pongSettings

	this.disable_ranges = function(disable) {
		settings.ball.disabled	= disable;
		settings.paddle1.disabled	= disable;
		settings.paddle2.disabled	= disable;
	}

	this.disable_all = function(disable) {
		console.log("Settings input disabled/enabled.");
		if (settings.difficulty.value > 3)
			this.disable_ranges(disable);
		settings.p1.disabled = disable;
		settings.difficulty.disabled = disable;
		settings.p1[0].disabled = disable;
		settings.p1[1].disabled = disable;
	}

	this.refresh = function() {
		this.multiplayer = settings.p1.value == "umano";
		switch (settings.difficulty.value) {
			case '1':
				this.ballspeed = 300;
				this.paddle1speed = this.multiplayer ? 500 : 150;
				this.paddle2speed = 500;
				console.log("Difficoltà impostata su facile.");
				this.disable_ranges(true);
			break;
			case '2':
				this.ballspeed = 500;
				this.paddle1speed = this.multiplayer ? 500 : 300;
				this.paddle2speed = 500;
				console.log("Difficoltà impostata su medio.");
				this.disable_ranges(true);
			break;
			case '3':
				this.ballspeed = 800;
				this.paddle1speed = 600;
				this.paddle2speed = 600;
				console.log("Difficoltà impostata su difficile.");
				this.disable_ranges(true);
			break;
			case '4':
				console.log("Difficoltà impostata manualmente.");
				this.disable_ranges(false);
				this.ballspeed = settings.ball.value;
				this.paddle1speed = settings.paddle1.value;
				this.paddle2speed = settings.paddle2.value;
			break;
		}
		if (settings.difficulty.value != 4) {
			settings.ball.value   	= this.ballspeed;
			settings.paddle1.value	= this.paddle1speed;
			settings.paddle2.value	= this.paddle2speed;
		}
		console.log("ballspeed = " + this.ballspeed + " px/s");
		console.log("paddle1speed = " + this.paddle1speed + " px/s");
		console.log("paddle2speed = " + this.paddle2speed + " px/s");
		settings.ballpxs.value = this.ballspeed;
		settings.paddle1pxs.value = this.paddle1speed;
		settings.paddle2pxs.value = this.paddle2speed;
	}
}

var usedKeys = [13, 87, 83, 38, 40, 122];
var Key = {
	_pressed: {},
	_repeated: {},

	LEFT: 37,
	UP: 38,
	RIGHT: 39,
	DOWN: 40,
	ENTER: 13,
	W: 87,
	S: 83,
	
	isDown: function(keyCode) {
		return this._pressed[keyCode];
	},
	
	isRepeated: function(keyCode) {
		return this._repeated[keyCode];
	},
	
	onKeydown: function(event) {
		//console.log(event.keyCode);
		if (usedKeys.indexOf(event.keyCode) > 0)
		{
			//prevent default browser action for game's key
			event.preventDefault();
			event.stopPropagation();
		}
		if (this._pressed[event.keyCode])
			this._repeated[event.keyCode] = true;
		else
			this._pressed[event.keyCode] = true;
	},
	
	onKeyup: function(event) {
		delete this._pressed[event.keyCode];
		delete this._repeated[event.keyCode];
	}
};

window.addEventListener('keyup', function(event) { Key.onKeyup(event); }, false);
window.addEventListener('keydown', function(event) { Key.onKeydown(event); }, false);

function Paddle(x, y, w, h, speed) {
	this.initial_x = x
	this.initial_y = y
	this.initial_w = w
	this.initial_h = h
	this.initial_speed = speed

	this.x = x
	this.y = y
	this.w = w
	this.h = h
	this.speed = speed

	this.moving = false
	this.dir = true

	this.reset = function() {
		this.x = this.initial_x
		this.y = this.initial_y
		this.w = this.initial_w
		this.h = this.initial_h
		this.speed = this.initial_speed
	};

	this.collision = function(x, y, w, h) {
		return (x <= this.x + this.w && x + w >= this.x && y + h >= this.y && y <= this.y + this.h)
	};
	this.isBounce = function(ball) {
		return this.collision(ball.x, ball.y, ball.size, ball.size)
	}

	this.moveDown = function(dt) {
		this.y += this.speed * dt
		this.moving = true
		this.dir = true

		//don't let paddles go outside field
		if (this.y >= canvas.height - this.h)
			this.y = canvas.height - this.h
	}

	this.moveUp = function(dt) {
		this.y -= this.speed * dt
		this.moving = true
		this.dir = false

		//don't let paddles go outside field
		if (this.y <= 0)
			this.y = 0;
	}

	this.draw = function(ctx) {
		ctx.fillRect(Math.floor(this.x), Math.floor(this.y), this.w, this.h);
	}
	
	this.isBallGoingTowardMe = function(ball) {
		if (ball.x > this.x)
			return !ball.goingRight()
		else
			return ball.goingRight()
	}
}

function Ball(x, y, size, speed) {
	this.initial_x = x
	this.initial_y = y
	this.initial_size = size
	this.initial_speed = speed
	
	this.x = x
	this.y = y
	this.size = size
	this.speed = speed	//speed in pixels per second, modify this to change ball's speed
	this.angle = 0
	this.maxAngle = Math.PI / 3

	this.randomAngle = function() {
		//give the ball a random direction
		this.angle = getRandomArbitrary(-this.maxAngle, this.maxAngle);
		if (Math.random() >= 0.5) {
			if (this.angle < 0)
				this.angle = -Math.PI - this.angle;
			else
				this.angle = Math.PI - this.angle;
		}
		console.log("Random angle: " + rad2deg(this.angle).toFixed(1) + "°");
	}

	this.reset = function() {
		this.x = this.initial_x
		this.y = this.initial_y
		this.size = this.initial_size
		this.speed = this.initial_speed
		this.randomAngle()
	}

	this.draw = function(ctx) {
		ctx.fillRect(Math.floor(this.x), Math.floor(this.y), this.size, this.size);
	}

	this.goingRight = function() {
		return this.angle > (-Math.PI / 2) && (this.angle < Math.PI / 2)
	}
};

function Player(paddle, AI_enabled, up_key, down_key) {
	this.score = 0;
	this.paddle = paddle
	this.ready = false;
	this.AI_enabled = AI_enabled;
	this.up_key = up_key;
	this.down_key = down_key;
	
	this.AI = function(dt, ball, height) {
		if (this.paddle.isBallGoingTowardMe(ball))
			//computes how far is the paddle from the ball
			var delta = Math.floor((this.paddle.y + this.paddle.h / 2) - (ball.y + ball.size / 2));
		else
			//computes how far is the paddle from the centre of canvas
			var delta = Math.floor((this.paddle.y + this.paddle.h / 2) - (height / 2));

		if (delta > 25)		//give an offset of 25
			this.paddle.moveUp(dt);
		else if (delta < -25)
			this.paddle.moveDown(dt);
	}
	
	this.getReady = function() {
		if (this.AI_enabled)
			this.ready = true;
		else if ((Key.isDown(this.up_key) && !Key.isRepeated(this.up_key)) ||
			 (Key.isDown(this.down_key) && !Key.isRepeated(this.down_key)))
			this.ready = true;
		return this.ready;
	}
	
	this.human = function(dt) {
		if (Key.isDown(this.up_key))
			this.paddle.moveUp(dt);
		if (Key.isDown(this.down_key))
			this.paddle.moveDown(dt);
	}
};

function Pong(canvas, basedir) {
	this.version = "0.9.3";

	this.canvas = canvas;
	window.addEventListener('keydown', function(event) {
		if (event.keyCode == 122) {
			makeFullscreen(canvas);
		}
	}, false);

	this.ctx = canvas.getContext("2d"); //WebGL2D.enable(canvas);

	this.settings = new Settings();

	this.ball = new Ball(Math.floor(canvas.width / 2 - 10), Math.floor(canvas.height / 2 - 10), 20, this.settings.ballspeed);

	paddle1 = new Paddle(50, Math.floor(canvas.height / 2 - 50), 20, 100, this.settings.paddle1speed);
	paddle2 = new Paddle(canvas.width - 70, Math.floor(canvas.height / 2 - 50), 20, 100, this.settings.paddle2speed);

	this.player1 = new Player(paddle1, !this.settings.multiplayer, Key.W, Key.S);
	this.player2 = new Player(paddle2, false, Key.UP, Key.DOWN);
	this.pings = 0;	//number of bounces against the field boundaries
	this.pongs = 0;	//number of bounces against the paddles
	this.ready = false;

	this.ping_sound = new Audio(basedir + 'sound/ping.ogg');
	this.pong_sound = new Audio(basedir + 'sound/pong.ogg');
	this.score_sound = new Audio(basedir + 'sound/score.ogg');

	this.update = function(dt) {
		if (!this.ready) {
			if (!this.player1.getReady() || !this.player2.getReady())
				return;
			else {
				this.ready = true;
				this.settings.disable_all(true); //disable settings inputs
			}
		}

		this.player1.paddle.moving = false;
		this.player2.paddle.moving = false;

		if (this.player1.AI_enabled) {
			this.player1.AI(dt, this.ball, this.canvas.height);
		} else {
			this.player1.human(dt)
		}
		this.player2.human(dt)

		this.ball.x += this.ball.speed * dt * Math.cos(this.ball.angle);
		this.ball.y += this.ball.speed * dt * Math.sin(this.ball.angle);

		if (this.ball.x + this.ball.size >= canvas.width) {
			this.player1.score++;	//Player1 scored!!!
			this.reset();
		} else if (this.ball.x <= 0) {
			this.player2.score++;	//Player2 scored!!!
			this.reset();
		}

		if (this.ball.y + this.ball.size > canvas.height) {
			this.pong()
			this.ball.y = canvas.height - this.ball.size;
		} else if (this.ball.y < 0) {
			this.pong();
			this.ball.y = 0;
		}

		if (this.player1.paddle.isBounce(this.ball))
			this.ping(this.player1.paddle)
		else if (this.player2.paddle.isBounce(this.ball))
			this.ping(this.player2.paddle)
	}

	this.draw = function() {
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);	//blacks everything

		this.ctx.fillStyle = "#FFFFFF";
		this.player1.paddle.draw(this.ctx)
		this.player2.paddle.draw(this.ctx)
		this.ball.draw(this.ctx)

		// FPS
		this.ctx.font = "12px Monospace";
		this.ctx.textAlign = "left";
		this.ctx.textBaseline = "top";
		
		this.ctx.fillText("FPS: " + fps.toFixed(2) + "   pings: " + this.pings + " pongs: " + this.pongs, 0, 0);

		this.ctx.textAlign = "center";
		this.ctx.font = "40px Imagine, Monospace";
		this.ctx.fillText(this.player1.score, this.canvas.width / 2 - 50, 24);	//player 1 score
		this.ctx.fillText(this.player2.score, this.canvas.width / 2 + 50, 24);	//player 2 score

		this.ctx.font = "24px Imagine, Monospace";
		if (!this.player1.ready)
			this.ctx.fillText("Press W or S", this.player1.paddle.x + 75, this.canvas.height - 50);
		if (!this.player2.ready)
			this.ctx.fillText("Press UP or DOWN", this.player2.paddle.x - 75, this.canvas.height - 50);

		//draws the middle dashed line
		var y = 0;
		while (y < this.canvas.height)
		{
			this.ctx.fillRect(this.canvas.width / 2 - 2, y, 4, 30);
			y += 50;
		}
	}

	this.pong = function() {
		this.ball.angle *= -1;
		this.pong_sound.play();
		this.pongs++;
		console.log("Pong! Angle: " + rad2deg(this.ball.angle).toFixed(1) + "°");
	}

	this.ping = function(paddle) {
		var ball_cx = this.ball.y + this.ball.size / 2;
		var paddle_cx = paddle.y + paddle.h / 2;
		var distance = ball_cx - paddle_cx;

		var h2 = Math.floor(paddle.h / 2 + this.ball.size / 2);

		var maxAngle = this.ball.maxAngle
		var angle = map(distance, -h2, h2, -maxAngle, maxAngle);

		if (paddle.moving)  //increase angle if paddle is moving
			if (paddle.dir)
				angle += Math.PI / 8;
			else
				angle -= Math.PI / 8;

		if (angle > maxAngle)
			angle = maxAngle;
		else if (angle < -maxAngle)
			angle = -maxAngle;

		if (paddle == this.player2.paddle)
			angle = (angle < 0) ? -Math.PI - angle : Math.PI - angle;

		this.ball.angle = angle
		console.log("Ping! Angle: " + rad2deg(angle).toFixed(1) + "°");

		this.pings++;	//PING!!!
		this.ping_sound.play();//PING!!!
	}

	var lastTime = 0;
	var applicationTime = 0;
	var frameCounter = 0, previousFrames = 0, fps = 0, fpsTime = 0;	//variables used to compute FPS
	var self = this;

	this.loop = function(time) {
		// register to be called again on next frame.
		requestAnimationFrame(self.loop);

		// compute time elapsed since last frame.
		var dt = time - lastTime;

		if (dt < 10) return;
		if (dt > 100) dt = 16;  // consider only 1 frame elapsed if unfocused.

		lastTime = time;
		applicationTime += dt;

		self.update(dt / 1000);
		self.draw();

		frameCounter++;

		var fpsdt = time - fpsTime;
		if (fpsdt >= 1000) {
			fps = (frameCounter - previousFrames) / fpsdt * 1000;
			previousFrames = frameCounter;
			fpsTime = time;
		}
	}

	this.reloadSettings = function() {
		if ((this.player1.ready && !this.player1.AI_enabled) || this.player2.ready)
			return;
		this.settings.refresh();
		this.ball.initial_speed = this.settings.ballspeed;
		this.player1.paddle.initial_speed = this.settings.paddle1speed;
		this.player2.paddle.initial_speed = this.settings.paddle2speed;
		this.player1.AI_enabled = !this.settings.multiplayer;
		this.player1.ready = this.player2.ready = false;
		this.player1.paddle.reset();
		this.player2.paddle.reset();
		this.ball.reset();
		console.log("Reloaded settings");
	}

	this.reset = function() {
		// Reset the game when the ball reaches the end of the field
		this.player1.ready = this.player2.ready = this.ready = false;
		this.reloadSettings();
		this.pings = 0;
		this.pongs = 0;
		this.settings.disable_all(false);
		this.score_sound.play();
	};

	this.restart = function() {
		this.player1.score = 0;
		this.player2.score = 0;
		this.reset();
		return false;
	}

	this.reset();
	requestAnimationFrame(this.loop);
}

